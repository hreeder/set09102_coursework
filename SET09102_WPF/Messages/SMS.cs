﻿using SET09102_WPF.Processors;
using System;

namespace SET09102_WPF.Messages
{
    public class SMS : Message
    {
        public SMS(String id, String sender, String body, string raw) : base(id, sender, body, raw)
        {
            this.AddProcessor(TextSpeakProcessor.Instance);
            this.process();
            this.Type = "SMS";
        }
    }
}
