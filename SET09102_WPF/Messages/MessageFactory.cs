﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace SET09102_WPF.Messages
{
    public class MessageFactory
    {
        /// <summary>
        /// Takes a message represented by a single string and constructs a Message
        /// </summary>
        /// <param name="message">String containing the message</param>
        /// <returns>The relevant message type for the supplied message</returns>
        public static Message construct_message(String message)
        {
            // Split by space to get the ID and body
            String id = message.Split(' ')[0];
            String body = String.Join(" ", message.Split(' ').Skip(1).ToArray());
            return construct_message(id, body);
        }

        /// <summary>
        /// Constructs a Message based on ID and Body
        /// </summary>
        /// <param name="id"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static Message construct_message(String id, String body)
        {
            id = id.ToUpper();

            // ID must match this pattern
            string idpattern = @"^[EST]\d{9}$";
            Regex idregex = new Regex(idpattern);
            if (!idregex.IsMatch(id))
            {
                throw new Exception("ID does not conform to a valid format (E/S/T plus 9 digits)");
            }

            if (id.StartsWith("E"))
            {
                // Email
                Message email;
                string sender;
                string subject;
                string text;

                // Get Sender & Subject
                sender = body.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                subject = body.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1];
                String[] tmpText = body.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Skip(2).ToArray();
                text = String.Join(Environment.NewLine, tmpText);

                // Validate the email address
                try
                {
                    MailAddress m = new MailAddress(sender);
                }
                catch (FormatException)
                {
                    throw new Exception("Email Address Invalid");
                }

                // Validate the body length
                if (text.Length > 1028)
                {
                    throw new Exception("Message text is too long");
                }

                // Detect SIR
                if (subject.StartsWith("SIR"))
                {
                    SIRNatures nature;
                    string natureLine;
                    string sortCode;
                    string sortCodeLine;
                    // First two lines of text will be
                    // Sort Code: 00-00-00
                    // Nature of Incident: Theft
                    // First line - sort code
                    sortCodeLine = text.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                    // Second line - nature
                    natureLine = text.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1];

                    // Extract the relevant info with regexes
                    string sortcodepattern = @"^Sort Code: (\d\d-\d\d-\d\d)$";
                    string naturepattern = @"^Nature of Incident: (.+)$";

                    if (Regex.IsMatch(sortCodeLine, sortcodepattern, RegexOptions.IgnoreCase))
                    {
                        sortCode = Regex.Match(sortCodeLine, sortcodepattern).Groups[1].Value;
                    }
                    else
                    {
                        throw new Exception("Sort Code Invalid");
                    }

                    if (Regex.IsMatch(natureLine, naturepattern, RegexOptions.IgnoreCase))
                    {
                        string match = Regex.Match(natureLine, naturepattern).Groups[1].Value;
                        // Nature requires us to remove spaces
                        match = match.Replace(" ", "");

                        // Cast from our string to the Enum containing natures
                        nature = (SIRNatures) Enum.Parse(typeof (SIRNatures), match);
                    }
                    else
                    {
                        throw new Exception("Nature Invalid");
                    }

                    if (subject.Length > 20)
                    {
                        throw new Exception("Subject too long");
                    }

                    email = new SIREmail(id, sender, text, subject, nature, sortCode, body);
                }
                else
                {
                    email = new EMail(id, sender, text, subject, body);
                }
                return email;
            }
            else if (id.StartsWith("T"))
            {
                // Tweet
                string sender = body.Split(' ')[0];
                string text = String.Join(" ", body.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray());

                if (!valid_twitter_handle(sender))
                {
                    throw new Exception("Sender is an invalid Twitter handle");
                }

                if (text.Length > 140)
                {
                    throw new Exception("Message text is too long");
                }

                Message tweet = new Tweet(id, sender, text, body);
                return tweet;
            }
            else if (id.StartsWith("S"))
            {
                // SMS
                string sender = body.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries)[0];
                string text = String.Join(" ", body.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray());

                // Pattern for handling numbers, including optional extension number
                string numberPattern = @"^([\+]?[0-9]{1,3}[\s.-]*[0-9]{1,12})([\s.-]*?[0-9]{1,4}?)$";

                if (!Regex.IsMatch(sender, numberPattern))
                {
                    throw new Exception("Sender was not a valid phone number!");
                }

                if (text.Length > 140)
                {
                    throw new Exception("Message text is too long");
                }

                Message sms = new SMS(id, sender, text, body);
                return sms;
            }
            return null;
        }

        /// <summary>
        /// Validates a twitter handle
        /// </summary>
        /// <param name="handle">The handle to validate</param>
        /// <returns>True if handle is valid</returns>
        public static Boolean valid_twitter_handle(string handle)
        {
            // Seperate function in the event we validate mentions

            // Handles must start with a @ and then have up to 15 characters
            if (!handle.StartsWith("@") || handle.Length > 16)
            {
                return false;
            }
            return true;
        }
    }
}
