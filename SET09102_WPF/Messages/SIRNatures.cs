﻿namespace SET09102_WPF.Messages
{
    public enum SIRNatures
    {
        ATMTheft = 00,
        BombThreat = 01,
        CashLoss = 02,
        CustomerAttack = 03,
        Intelligence = 04,
        Raid = 05,
        StaffAbuse = 06,
        SuspiciousIncident = 07,
        Terrorism = 08,
        Theft = 09,
    }
}
