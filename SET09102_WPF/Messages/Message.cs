﻿using SET09102_WPF.Processors;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SET09102_WPF.Messages
{
    public abstract class Message
    {
        private List<IMessageProcessor> processors;

        public Message(string id, string sender, string body, string raw)
        {
            this.processors = new List<IMessageProcessor>();
            this.ID = id;
            this.Sender = sender;
            this.Body = body;
            this.Received = DateTime.Now;
            this.Raw = raw;
        }

        public string ID { get; set; }
        public string Sender { get; set; }
        public string Body { get; set; }
        public string Type { get; set; }
        public DateTime Received { get; set; }
        public string Raw { get; set; }

        public void process()
        {
            foreach (IMessageProcessor processor in this.processors)
            {
                processor.process_message(this);
            }
        }

        public void AddProcessor(IMessageProcessor processor)
        {
            this.processors.Add(processor);
        }

        public string GetJson()
        {
            JsonSerializerSettings jss = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.All
            };
            return JsonConvert.SerializeObject(this, jss);
        }
    }
}
