﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace SET09102_WPF.Messages
{
    public class SIREmail : EMail
    {
        public static List<Tuple<String, SIRNatures>> SIRs = new List<Tuple<string, SIRNatures>>();
        public SIREmail(String id, String sender, String body, String subject, SIRNatures nature, String sortCode, string raw) : base(id, sender, body, subject, raw)
        {
            // We don't need to call this.process because that's called in EMail's constructor
            this.Nature = nature;
            this.SortCode = sortCode;
            this.Type = "SIR Email";
            SIRs.Add(new Tuple<string, SIRNatures>(this.SortCode, this.Nature));
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public SIRNatures Nature { get; set; }

        public string SortCode { get; set; }
    }
}
