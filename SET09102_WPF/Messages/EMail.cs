﻿using SET09102_WPF.Processors;
using System;

namespace SET09102_WPF.Messages
{
    public class EMail : Message
    {
        public EMail(String id, String sender, String body, String subject, string raw) : base(id, sender, body, raw)
        {
            this.Subject = subject;
            this.Type = "EMail";

            this.AddProcessor(URLQuarantineProcessor.Instance);
            this.process();
        }

        public string Subject { get; set; }
    }
}
