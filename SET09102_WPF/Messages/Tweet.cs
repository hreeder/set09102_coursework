﻿using SET09102_WPF.Processors;
using System;
using System.Collections.Generic;

namespace SET09102_WPF.Messages
{
    public class Tweet : Message
    {
        public Tweet(String id, String sender, String body, string raw) : base(id, sender, body, raw)
        {
            this.AddProcessor(TweetProcessor.Instance);
            this.AddProcessor(TextSpeakProcessor.Instance);

            this.Hashtags = new List<String>();
            this.Mentions = new List<String>();

            this.Type = "Tweet";

            this.process();
        }

        public List<String> Hashtags { get; set; }

        public List<String> Mentions { get; set; }
    }
}
