﻿using SET09102_WPF.Messages;

namespace SET09102_WPF.Processors
{
    public interface IMessageProcessor
    {
        void process_message(Message message);
    }
}
