﻿using System;
using System.Collections.Generic;
using SET09102_WPF.Messages;

namespace SET09102_WPF.Processors
{
    class URLQuarantineProcessor : IMessageProcessor
    {
        private static URLQuarantineProcessor instance { get; set; }
        public static URLQuarantineProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new URLQuarantineProcessor();
                    quarantinedURLs = new List<String>();
                }
                return instance;
            }
        }

        private static List<String> quarantinedURLs { get; set; }

        public void process_message(Message message)
        {
            String body = message.Body;
            String[] lines = body.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            List<String> outputLines = new List<String>();

            // Loop through all lines in the message
            foreach(String line in lines)
            {
                String[] words = line.Split(' ');

                List<String> outputWords = new List<String>();

                // Loop through all words in the message
                foreach (String word in words)
                {
                    // If the word starts with a http:// or https:// quarantine it
                    if (word.StartsWith("http://") || word.StartsWith("https://"))
                    {
                        URLQuarantineProcessor.quarantinedURLs.Add(word);
                        outputWords.Add("<URL Quarantined>");
                    }
                    else
                    {
                        outputWords.Add(word);
                    }
                }
                // Join the words from this line to create a line
                outputLines.Add(String.Join(" ", outputWords.ToArray()));
            }

            // Join all the lines to create the body
            message.Body = String.Join("\r\n", outputLines.ToArray());
        }
    }
}
