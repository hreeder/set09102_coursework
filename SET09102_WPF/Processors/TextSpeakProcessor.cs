﻿using System;
using System.Collections.Generic;
using System.IO;
using SET09102_WPF.Messages;

namespace SET09102_WPF.Processors
{
    class TextSpeakProcessor : IMessageProcessor
    {

        private static TextSpeakProcessor instance;
        public static TextSpeakProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TextSpeakProcessor();
                }
                return instance;
            }
        }

        public void process_message(Message message)
        {
            // Read all the abbreviations in to a dictionary
            StreamReader reader = new StreamReader(File.OpenRead("C:\\dev\\set09102_coursework\\SET09102_WPF\\textwords.csv"));
            Dictionary<string, string> textspeak = new Dictionary<string, string>();

            while (!reader.EndOfStream)
            {
                String line = reader.ReadLine();
                String[] values = line.Split(',');

                textspeak.Add(values[0], values[1]);
            }

            String body = message.Body;
            String[] lines = body.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            List<String> outputLines = new List<String>();
            foreach (String line in lines)
            {
                String[] words = line.Split(' ');

                List<String> outputWords = new List<String>();

                // Loop through all words in the existing body
                // and replace words where necessary
                foreach (String word in words)
                {
                    String expansion;
                    if (textspeak.TryGetValue(word, out expansion))
                    {
                        outputWords.Add(word + " <" + expansion + ">");
                    }
                    else
                    {
                        outputWords.Add(word);
                    }
                }
                outputLines.Add(String.Join(" ", outputWords.ToArray()));
            }

            // Replace our message body with the processed version
            message.Body = String.Join("\r\n", outputLines.ToArray());
        }
    }
}
