﻿using System;
using System.Collections.Generic;
using SET09102_WPF.Messages;

namespace SET09102_WPF.Processors
{
    public class TweetProcessor : IMessageProcessor
    {
        private static TweetProcessor instance;
        public static TweetProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TweetProcessor();
                }
                return instance;
            }
        }

        // Have a dict to store our hashtags
        public static Dictionary<string, int> Hashtags = new Dictionary<string, int>();

        public void process_message(Message message)
        {
            Tweet tweet = (Tweet) message;
            string body = message.Body;
            string[] lines = body.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            // Split the message by line and word
            foreach (string line in lines)
            {
                string[] words = line.Split(' ');

                foreach (string word in words)
                {
                    // If a word starts with an @, it's a mention
                    if (word.StartsWith("@"))
                    {
                        tweet.Mentions.Add(word);
                    }
                    // If a word starts with a # it's a hashtag
                    else if (word.StartsWith("#"))
                    {
                        tweet.Hashtags.Add(word);

                        int current;
                        // Add one to our current count of occurrences of this hashtag
                        if (Hashtags.TryGetValue(word, out current))
                        {
                            Hashtags[word] = current + 1;
                        }
                        else
                        {
                            Hashtags.Add(word, 1);
                        }
                    }
                }
            }
        }
    }
}
