SET09102 - Software Engineering
40052308 - Harry Reeder

# Projects
This solution contains three projects
* SET09102_ASPNET
	This contains the view code for the ASP.NET site developed for NBMFS
* SET09102_WPF
	This contains the back-end entity classes, factory and processors required for the project.
	It also contains what was begun as my WPF implementation, before I decided to go down the 
	ASP.NET route.
* SET09102_Coursework
	This contains my Class and Activity diagrams

# ASP.NET Project Structure
If you are unfamiliar with ASP.NET, the structure is as follows. ASP.NET follows a pretty standard
MVC pattern, and as such you will find elements named as such.
The Controllers folder contains the first code run when a user access a page. HomeController.cs
contains the default index, while MessageController.cs contains any actions that start with /Message/.
The Views folder contains all of the HTML for the application. The overall template is in Shared/_Layout.cshtml,
while the other folders in Views correspond to the relevant controllers. Within each, you will find a .cshtml
file that corresponds to the relevant action within the controller, ie Views/Home/Index.cshtml corresponds to the
Index action of Home Controller.

# Files to Note
SET09102_ASPNET/Controllers/MessageController.cs
SET09102_ASPNET/MessageContainer.cs
SET09102_WPF/Messages/MessageFactory.cs