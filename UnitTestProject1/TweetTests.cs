﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SET09102_WPF.Messages;

namespace UnitTestProject1
{
    [TestClass]
    public class TweetTests
    {
        private Tweet tweet = null;

        [TestInitialize]
        public void Create()
        {
            this.tweet = (Tweet) MessageFactory.construct_message("T123456789 @tweeter Test Tweet LOL @rkemmer #hashtag");
        }

        [TestMethod]
        public void TestTweetHasId()
        {
            Assert.AreEqual("T123456789", this.tweet.ID);
        }

        [TestMethod]
        public void TestTweetHasSender()
        {
            Assert.AreEqual("@tweeter", this.tweet.Sender);
        }

        [TestMethod]
        public void TestTweetHasBody()
        {
            Assert.AreEqual(true, this.tweet.Body.StartsWith("Test Tweet"));
        }

        [TestMethod]
        public void TestTextSpeakProcessorApplies()
        {
            Assert.AreEqual(true, this.tweet.Body.Contains("LOL <Laughing out loud>"));
        }

        [TestMethod]
        public void TestMentionsContainsRKemmer()
        {
            Assert.AreEqual(true, this.tweet.Mentions.Contains("@rkemmer"));
        }

        [TestMethod]
        public void TestHashtagsContainsHashtag()
        {
            Assert.AreEqual(true, this.tweet.Hashtags.Contains("#hashtag"));
        }
    }
}
