﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SET09102_WPF.Messages;

namespace UnitTestProject1
{
    [TestClass]
    public class TypeTests
    {
        [TestMethod]
        public void TestSmsGeneration()
        {
            string testSmsMessage = "S123456789 +447415722341 This is a test SMS";
            Message message = MessageFactory.construct_message(testSmsMessage);

            Assert.IsInstanceOfType(message, typeof(SMS));
        }

        [TestMethod]
        public void TestTweetGeneration()
        {
            string testTweetMessage = "T123456789 @hreeder This is a test Tweet";
            Message message = MessageFactory.construct_message(testTweetMessage);

            Assert.IsInstanceOfType(message, typeof(Tweet));
        }

        [TestMethod]
        public void TestEmailGeneration()
        {
            string testEmailMessage = @"E123456789 sender@example.org
Subject Line Goes Here
This is now a body";
            Message message = MessageFactory.construct_message(testEmailMessage);

            Assert.IsInstanceOfType(message, typeof(EMail));
        }

        [TestMethod]
        public void TestSirEmailGeneration()
        {
            string testSirEmailMessage = @"E123456789 sender@example.org
SIR 08/09/15
Nature of Incident: Theft
Sort Code: 08-09-15
Message Body";
            Message message = MessageFactory.construct_message(testSirEmailMessage);

            Assert.IsInstanceOfType(message, typeof(SIREmail));
        }
    }
}
