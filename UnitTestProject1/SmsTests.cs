﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SET09102_WPF.Messages;

namespace UnitTestProject1
{
    [TestClass]
    public class SmsTests
    {
        private Message sms = null;

        [TestInitialize]
        public void Create()
        {
            this.sms = MessageFactory.construct_message("S123456789 +441234567890 Test SMS LOL");
        }

        [TestMethod]
        public void TestSmsHasId()
        {
            Assert.AreEqual("S123456789", this.sms.ID);
        }

        [TestMethod]
        public void TestSmsHasSender()
        {
            Assert.AreEqual("+441234567890", this.sms.Sender);
        }

        [TestMethod]
        public void TestSmsHasBody()
        {
            Assert.AreEqual(true, this.sms.Body.StartsWith("Test"));
        }

        [TestMethod]
        public void TestTextSpeakProcessorApplies()
        {
            Assert.AreEqual(true, this.sms.Body.Contains("LOL <Laughing out loud>"));
        }
    }
}
