﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using SET09102_WPF.Messages;

namespace SET09102_ASPNET
{
    public class MessageContainer
    {
        private static List<Message> _messages = new List<Message>();

        public static List<Message> Messages => _messages;


        public static void AddMessage(Message newMessage)
        {
            _messages.Add(newMessage);
        }

        public static List<SIREmail> GetSIRs()
        {
            return _messages.Where(message => message.GetType() == typeof (SIREmail)).Cast<SIREmail>().ToList();
        }
    }
}