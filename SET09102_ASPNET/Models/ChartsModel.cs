﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNet.Highcharts;

namespace SET09102_ASPNET.Models
{
    public class ChartsModel
    {
        public Highcharts Chart1 { get; set; }
        public Highcharts Chart2 { get; set; }
    }
}