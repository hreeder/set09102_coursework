﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SET09102_WPF.Messages;
using SET09102_WPF.Processors;

namespace SET09102_ASPNET.Controllers
{
    public class HomeController : Controller
    {
        // GET /
        // GET /Home/Index
        // Displays the homepage
        public ActionResult Index()
        {
            // Message Counter
            ViewBag.MessageCount = MessageContainer.Messages.Count;

            // Trending Hashtags
            List<KeyValuePair<string, int>> hashtagList = TweetProcessor.Hashtags.ToList();
            hashtagList.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            hashtagList.Reverse();
            ViewBag.Hashtags = hashtagList.Take(10);

            // SIR Display
            ViewBag.SIRs = MessageContainer.GetSIRs();

            return View();
        }
    }
}