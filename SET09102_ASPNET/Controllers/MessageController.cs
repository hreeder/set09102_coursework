﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using Newtonsoft.Json;
using SET09102_ASPNET.Models;
using SET09102_WPF.Messages;
using SET09102_WPF.Processors;

namespace SET09102_ASPNET.Controllers
{
    public class MessageController : Controller
    {
        // GET: Message
        // Displays list of all messages
        public ActionResult Index()
        {
            List<Message> messages = MessageContainer.Messages;
            // Sort by datetime received, newest last
            messages.Sort((firstMessage, nextMessage) => firstMessage.Received.CompareTo(nextMessage.Received));
            // Add this to our viewbag for display
            ViewBag.Messages = messages;
            return View();
        }

        // GET: Message/ByType/Type
        // Displays list of all messages of type
        public ActionResult ByType(string id)
        {
            // Get all messages of the specific type, and order by Received
            List<Message> messages = (from msg in MessageContainer.Messages
                where msg.Type.Replace(" ", "").ToLower().Equals(id.ToLower())
                select msg)
                .OrderBy(message => message.Received)
                .ToList();

            // Add the following to our viewbag and render with the Index view
            ViewBag.Messages = messages;
            ViewBag.ByType = true;
            ViewBag.RequestedType = id;
            return View("Index");
        }

        // GET: Message/Details/T111111111
        // Displays an individual message by ID
        public ActionResult Details(string id)
        {
            Message target = null;
            List<Message> messages = MessageContainer.Messages;

            // Select the message given ID - default to null (if we don't find the ID)
            target = (from msg in messages where msg.ID.Equals(id) select msg)
                .DefaultIfEmpty(null)
                .First();

            // If the message wasn't found, return a 404
            if (target == null)
            {
                return HttpNotFound();
            }
            else
            {
                // If we've got a message, we're going to cast it to the relevant type so we can access
                // the type specific variables
                Message message;
                if (target.GetType() == typeof(EMail))
                {
                    message = (EMail)target;
                }
                else if (target.GetType() == typeof(SIREmail))
                {
                    message = (SIREmail) target;
                }
                else if (target.GetType() == typeof(Tweet))
                {
                    message = (Tweet)target;
                }
                else if (target.GetType() == typeof(SMS))
                {
                    message = (SMS)target;
                }
                // Default fallthrough case, in the event that we've added a new type of message but forgot
                // to have it added to the controller
                else
                {
                    message = target;
                }

                // Finally, add the message to the ViewBag to be able to display it
                ViewBag.Message = message;
            }
            return View();
        }

        // GET: Message/ExportAll
        public ActionResult ExportAll()
        {
            // Get all messages
            List<Message> messages = MessageContainer.Messages;
            JsonSerializerSettings jss = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.All
            };

            // Drop the entire list of messages to a JSON string
            string jsonMessages = JsonConvert.SerializeObject(messages, jss);

            // Now return that as a file
            using (var fileStream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(fileStream))
                {
                    writer.Write(jsonMessages);
                }
                return File(
                    new MemoryStream(fileStream.GetBuffer()), 
                    "application/json",
                    "NBMFS-messages.json"
                );
            }
        }

        // GET: Message/Export/<id>
        public ActionResult Export(string id)
        {
            Message target = null;
            List<Message> messages = MessageContainer.Messages;

            // Select the message given ID - default to null (if we don't find the ID)
            target = (from msg in messages where msg.ID.Equals(id) select msg)
                .DefaultIfEmpty(null)
                .First();

            // If the message wasn't found, return a 404
            if (target == null)
            {
                return HttpNotFound();
            }
            else
            {
                // Use a filestream to create a file with the contents of message.GetJson()
                using (var fileStream = new MemoryStream())
                {
                    using (StreamWriter writer = new StreamWriter(fileStream))
                    {
                        writer.Write(target.GetJson());
                    }
                    return File(
                        new MemoryStream(fileStream.GetBuffer()),
                        "application/json",
                        "NBMFS-" + target.ID + ".json"
                    );
                }
            }
            return View();
        }

        // GET: Message/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Message/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            // Get the header and message from our form
            string header = collection.GetValue("header").AttemptedValue;
            string message = collection.GetValue("message").AttemptedValue;

            // Attempt to construct a new message from this
            Message newMessage = null;
            try
            {
                newMessage = MessageFactory.construct_message(header, message);
            }
            catch (Exception e)
            {
                ViewBag.Error = e;
                return View();
            }

            // Throw a simple 500 if we weren't able to create a message, else add it to our
            // collection of messages
            if (newMessage != null)
            {
                MessageContainer.AddMessage(newMessage);
            }
            else
            {
                throw new ApplicationException();
            }

            // If we've successfully created a new message, redirect to the details page for this message
            return RedirectToAction("Details", "Message", new { id = newMessage.ID });
        }

        public ActionResult Import()
        {
            return View();
        }

        // Handles POSTing to /Message/Import
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase single, HttpPostedFileBase multiple)
        {
            // We care about types
            JsonSerializerSettings jss = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            // If we've got a single message file
            if (single != null)
            {
                using (var sr = new StreamReader(single.InputStream))
                {
                    // Read the message in, using the base class Message
                    // Because of TypeNameHandling.All, Json.NET will automatically
                    // cast to the right types as opposed to just keeping everything as a message
                    Message msg = JsonConvert.DeserializeObject<Message>(sr.ReadToEnd(), jss);
                    MessageContainer.AddMessage(msg);
                }
            }

            if (multiple != null)
            {
                using (var sr = new StreamReader(multiple.InputStream))
                {
                    // Same again, just for multiple
                    List<Message> messages = JsonConvert.DeserializeObject<List<Message>>(sr.ReadToEnd(), jss);
                    MessageContainer.Messages.AddRange(messages);
                }
            }

            // Redirect to showing all messages
            return RedirectToAction("Index", "Message");
        }

        public ActionResult Stats()
        {
            var messages = MessageContainer.Messages;


            // See the HighCharts API for documentation on what's going on here
            Highcharts typeCounts = new Highcharts("typecounts")
                .InitChart(new Chart { PlotShadow = false})
                .SetTitle(new Title {  Text = "Message Types" })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Count",
                    Data = new Data(new object[]
                    {
                        new object[] { "SMS", (from msg in messages where msg.Type.Equals("SMS") select msg).ToList().Count() },
                        new object[] { "Tweet", (from msg in messages where msg.Type.Equals("Tweet") select msg).ToList().Count() },
                        new object[] { "EMail", (from msg in messages where msg.Type.Equals("EMail") select msg).ToList().Count() },
                        new object[] { "SIR", (from msg in messages where msg.Type.Equals("SIR Email") select msg).ToList().Count() }
                    })
                });

            // Get all hashtags, sort by their popularity, biggest first
            var hashtagData = new Collection<Series>();
            List<KeyValuePair<string, int>> hashtagList = TweetProcessor.Hashtags.ToList();
            hashtagList.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            hashtagList.Reverse();

            // Take the first 20 to display
            foreach (KeyValuePair<string, int> tag in hashtagList.Take(20))
            {
                hashtagData.Add(new Series { Name = tag.Key, Data = new Data(new object[] { tag.Value })});
            }

            Highcharts hashtagCount = new Highcharts("hashtagcount")
                .InitChart(new Chart {Type = ChartTypes.Column})
                .SetTitle(new Title {Text = "Trending HashTags"})
                .SetXAxis(new XAxis {Categories = new[] { "Hashtags" }})
                .SetYAxis(new YAxis {Title = new YAxisTitle {Text = "Occurences"}})
                .SetTooltip(new Tooltip { Formatter = @"function() { return ''+ this.series.name +': '+ this.y; }" })
                .SetSeries(hashtagData.ToArray());

            ChartsModel charts = new ChartsModel
            {
                Chart1 = typeCounts,
                Chart2 = hashtagCount
            };

            return View(charts);
        }
    }
}
